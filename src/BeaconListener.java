package src;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface BeaconListener extends Remote {

    public int deposit(Beacon b) throws RemoteException;    // client will invoke this method to deposit a beacon to the server

}