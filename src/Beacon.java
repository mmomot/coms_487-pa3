package src;

public class Beacon {
    public int ID;
    public long StartUpTime;
    public String CmdAgentID;

    Beacon(int id, long time, String name){
        this.ID = id;
        this.StartUpTime = time;
        this.CmdAgentID = name;
    }
}