package src;

import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.RemoteRef;
import java.rmi.server.UnicastRemoteObject;

public class BeaconServer{

    public static void main(String args[]) {
        try {
            
            // Instantiate the implementation class
            BeaconListenerImpl beacon_service = new BeaconListenerImpl();

            RemoteRef location = beacon_service.getRef();

            System.out.println("REMOTE REF LOCATION: " + location.toString());

            String registry = "localhost";  // Could be something else on pyrite?

            String registration = "rmi://" + registry + "/BeaconListener";

            // Bind BeaconListenerImpl object to th registration URL for clients to see
            Naming.rebind(registration, beacon_service);
            
            System.out.println("Beacon Server Ready!");

        }catch( Exception e){
            System.err.println("Server exception: " + e.toString()); 
            e.printStackTrace(); 
        }
    }
    
}
