package src;

import java.rmi.*;
import java.util.Random;

public class Client extends Thread{
    private static Random rando = new Random(System.currentTimeMillis());

    public static void main(String args[]) {
        try{
            String registry = "localhost";

            String registration = "rmi://" + registry + "/BeaconListener";
    
            Remote remoteService = Naming.lookup( registration );

            BeaconListener beacon_service = (BeaconListener) remoteService;

            while(true){
                beacon_service.deposit( gen_beacon() );
                System.out.println("Beacon Sent!?");
                Thread.sleep(5000);
            }


        }
        catch (NotBoundException nbe) { System.out.println ("No light bulb service available in registry!"); } 
        catch (RemoteException re) { System.out.println ("RMI - " + re); }
        catch (Exception e) { System.out.println ("Error - " + e); }


    }

    private static Beacon gen_beacon(){
        long time = get_current_time();
        int id = gen_ID();
        String name = gen_name(id);
        Beacon b = new Beacon(id, time, name);
        System.out.println("Beacon ID: " + id + "      Current Time: " + time);
        return b;
    }

    private static long get_current_time(){
        long current_time = System.currentTimeMillis() / 1000l;
        return current_time;
    }
    
    private static int gen_ID(){
        return rando.nextInt( 1000 );
    }

    private static String gen_name(int id){
        String name = "Agent_" + id;
        return name;
    }

}
