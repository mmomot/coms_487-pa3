package src;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

public class BeaconListenerImpl extends java.rmi.server.UnicastRemoteObject implements BeaconListener {


    protected BeaconListenerImpl() throws RemoteException {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Override
    public int deposit(Beacon b) throws RemoteException {
        // TODO Auto-generated method stub

        Agent agent = new Agent(b.ID, b.StartUpTime, b.CmdAgentID);

        System.out.println("Beacon received from: " + b.CmdAgentID);

        //  Somehow add agent to the list of agents, should it be in here or the server?

        return 0;
    }

    
}

class AgentMonitor extends Thread
{
    private Map<Integer, Agent> agents = new HashMap<Integer, Agent>();

    public void run() 
    { 
        try
        { 
            while(true){
                //check for dead agents every 5 seconds
                check_agents();
                Thread.sleep(5000);
            }
        } 
        catch (Exception e) 
        { 
            System.out.println ("Exception is caught"); 
        } 
    } 

    public void update_agent(Agent agent){
        int agent_id = agent.get_ID();
        long current_time = System.currentTimeMillis() / 1000l; // Current time in seconds
        agents.get(agent_id).set_last_received(current_time);
        if(agents.get(agent_id).get_startup_time() != agent.get_startup_time()){
            //  New Agent
            System.out.println("Agent with same ID and different start time detected!");
            agents.put(agent_id, agent);
        }
    }

    private void check_agents()
    {
        long current_time = System.currentTimeMillis() / 1000l;

        for(Map.Entry<Integer, Agent> agent : agents.entrySet() ){
            long dead_time = agent.getValue().get_last_received() + ( 2 * agent.getValue().get_interval() );
            if( current_time > dead_time){
                // This agent is assumed to be dead, print message and remove from list
                System.out.println("AGENT " + agent.getKey() + " IS DEAD");
                agents.remove(agent.getValue().get_ID());
            }
        }
    }

    public boolean add_agent(Agent agent){
        int id = agent.get_ID();

        if(agents.containsKey(id)){
            return false;        
        }
        else{
            agents.put(agent.get_ID(), agent);
        return true;
        }
    }
}